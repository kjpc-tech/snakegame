#!/usr/bin/python3

# Author - Kyle Johnson
# snakeGame.py
# 18 MAR 16

import sys
import os
import random
import sqlite3
import datetime

if sys.version_info[0] < 3:
	print("Error; Python 3 required!")
	sys.exit()

try:
	import pyglet
	#from pyglet.window import mouse
	from pyglet.window import key
	from pyglet.gl import *
except ImportError:
	print("Error; pyglet required!")
	sys.exit()

FONT_NAME = "Comic Sans"
FONT_SIZE_LARGE = 56
FONT_SIZE_NORMAL = 36
FONT_SIZE_SMALL = 12

COLOR_SNAKE = (0, 250, 10)
COLOR_FOOD = (150, 0, 0)

STATE_MENU = 1
STATE_GAME_RUNNING = 2
STATE_GAME_PAUSED = 3

dbExisted = True
# create the DB file
if not "snakeGameDB.sqlite3" in os.listdir():
	dbExisted = False
	open("snakeGameDB.sqlite3", 'w').close()

# connect to the DB
dbConnection = sqlite3.connect("snakeGameDB.sqlite3")
dbCursor = dbConnection.cursor()

if not dbExisted:
	# create the table if we made a new file
	dbCursor.execute('''CREATE TABLE scores
             (timestamp text, name text, length integer, foodate integer)''')
	dbConnection.commit()

# function to get the high scores from the database
def getScores():
	dbCursor.execute("SELECT * FROM scores ORDER BY length")
	return list(reversed(dbCursor.fetchall()))[:15]

# SnakeGame class
class SnakeGame(pyglet.window.Window):
	def __init__(self, *args, **kwargs):
		pyglet.window.Window.__init__(self, *args, **kwargs)

		glEnable(GL_BLEND)
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

		self.snakeSize = 10
		self.maxSnakeSize = self.snakeSize
		self.foodsAte = 0

		self.dx = 0
		self.dy = 1

		self.turned = False
		self.movedAfterTurn = False

		self.gameState = STATE_MENU

		self.squareSize = self.height / 40

		self.pieces = []
		self.foods = []

		self.name = ["_", "_", "_"]

		self.setupGame()

		self.hsLabels = []
		self.makeHSLabels()

		self.menu_title = pyglet.text.Label(
			"Snake Game",
			font_name=FONT_NAME,
			font_size=FONT_SIZE_LARGE,
			bold=True,
			anchor_x='center',
			anchor_y='center',
			x=self.width // 2,
			y=self.height - 60
			)
		self.menu_title.color = (0, 250, 10, 255)
		self.menu_instuctions = pyglet.text.Label(
			"",
			font_name=FONT_NAME,
			font_size=FONT_SIZE_NORMAL,
			anchor_x='center',
			anchor_y='center',
			x=self.width // 2,
			y=self.height - 160
			)
		self.menu_instuctions.color = (180, 180, 180, 255)
		self.menu_status = pyglet.text.Label(
			"Paused",
			font_name=FONT_NAME,
			font_size=FONT_SIZE_NORMAL,
			bold=True,
			anchor_x='center',
			anchor_y='center',
			x=self.width // 2,
			y=self.height - 240
			)
		self.menu_status.color = (0, 255, 225, 255)
		self.menu_name = pyglet.text.Label(
			"",
			font_name=FONT_NAME,
			font_size=FONT_SIZE_NORMAL,
			anchor_x='center',
			anchor_y='center',
			x=self.width // 2,
			y=self.height - 300
			)
		self.menu_name.color = (255, 255, 225, 255)

		self.game_label = pyglet.text.Label(
			"",
			font_name=FONT_NAME,
			font_size=FONT_SIZE_SMALL,
			x=10,
			y=10
			)
		self.game_label.color = (0, 255, 225, 255)

		#self.fps_display = pyglet.clock.ClockDisplay()

	def setupGame(self):
		self.dx = 0
		self.dy = 1

		self.snakeSize = 10
		self.maxSnakeSize = self.snakeSize
		self.foodsAte = 0

		self.pieces = []
		self.foods = []

		self.addPiece = False
		self.losePiece = False

		# generate the snake
		x = (self.width / 2) - (self.squareSize / 2)
		y = self.height / 4
		for i in range(10):
			self.pieces.append((x, y))
			y -= self.squareSize

		for i in range(2):
			self.makeFood()

	def makeHSLabels(self):
		self.hsLabels = []
		scores = getScores()
		x = self.width // 4
		y = self.height - 400
		for s in range(len(scores)):
			if s in [5, 10]:
				x += self.width // 4
				y = self.height - 400
			self.hsLabels.append(pyglet.text.Label(
				"{0}: {1} - {2}".format(s + 1, scores[s][1].upper(), scores[s][2]),
				font_name=FONT_NAME,
				font_size=FONT_SIZE_SMALL,
				anchor_x='center',
				anchor_y='center',
				color=(255, 255, 255, 255),
				x=x,
				y=y
				)
			)
			y -= 30

	# main game loop function
	def mainloop(self, dt):
		if self.gameState == STATE_GAME_RUNNING:
			if self.addPiece:
				self.snakeSize += 1
				if self.snakeSize > self.maxSnakeSize:
					self.maxSnakeSize = self.snakeSize
				self.addPiece = False
			else:
				self.pieces.pop()
				if len(self.pieces) == 0:
					self.gameState = STATE_MENU
					dbCursor.execute("INSERT INTO scores VALUES (?,?,?,?)", (datetime.datetime.now().isoformat(), " ".join(self.name), self.maxSnakeSize, self.foodsAte))
					dbConnection.commit()
					self.makeHSLabels()
					self.setupGame()
					pyglet.clock.unschedule(self.mainloop)

			if self.gameState == STATE_GAME_RUNNING:
				if (self.dx == -1 and self.pieces[0][0] - self.squareSize < 0) or (self.dx == 1 and self.pieces[0][0] + self.squareSize >= self.width):
					self.losePiece = True
				elif (self.dy == -1 and self.pieces[0][1] - self.squareSize < 0) or (self.dy == 1 and self.pieces[0][1] + self.squareSize >= self.height):
					self.losePiece = True
				elif (self.pieces[0][0] + (self.squareSize * self.dx), self.pieces[0][1] + (self.squareSize * self.dy)) in self.pieces:
					self.losePiece = True
				if not self.losePiece:
					self.pieces.insert(0, (self.pieces[0][0] + (self.squareSize * self.dx), self.pieces[0][1] + (self.squareSize * self.dy)))
				else:
					self.snakeSize -= 1
					self.losePiece = False
				if self.pieces[0] in self.foods:
					self.addPiece = True
					self.foods.remove(self.pieces[0])
					self.foodsAte += 1
					self.makeFood()
			if self.turned:
				self.turned = False

	# window on draw function
	def on_draw(self):
		self.clear()
		#self.fps_display.draw()
		if self.gameState == STATE_MENU:
			self.menu_title.draw()
			self.menu_instuctions.text = "Press Space to Play"
			self.menu_instuctions.draw()
			self.menu_name.text = "Name: {0}".format(" ".join(self.name).upper())
			self.menu_name.draw()
			for hsLabel in self.hsLabels:
				hsLabel.draw()
		elif self.gameState == STATE_GAME_PAUSED:
			self.menu_title.draw()
			self.menu_instuctions.text = "Press Space to Continue"
			self.menu_instuctions.draw()
			self.menu_status.draw()
			self.game_label.text = "Your Current Size: {0}, Longest Size: {1}; Food Ate: {2}".format(self.snakeSize, self.maxSnakeSize, self.foodsAte)
			self.game_label.draw()
		elif self.gameState == STATE_GAME_RUNNING:
			self.game_label.text = "Your Current Size: {0}, Longest Size: {1}; Food Ate: {2}".format(self.snakeSize, self.maxSnakeSize, self.foodsAte)
			self.game_label.draw()
			#glClear(GL_COLOR_BUFFER_BIT)
			#glLoadIdentity()
			for food in self.foods:
				self.drawSquare(food[0], food[1], self.squareSize, COLOR_FOOD)
			for piece in self.pieces:
				self.drawSquare(piece[0], piece[1], self.squareSize, (0, 255, 0))
			head = self.pieces[0]
			eyeSize = self.squareSize / 3
			eyePadding = self.squareSize / 8
			if self.dx == -1 or self.dy == 1:
				self.drawSquare(head[0] + eyePadding, head[1] + self.squareSize - eyeSize - eyePadding, eyeSize, (0, 0, 255))
			if self.dy == 1 or self.dx == 1:
				self.drawSquare(head[0] + self.squareSize - eyeSize - eyePadding, head[1] + self.squareSize - eyeSize - eyePadding, eyeSize, (0, 0, 255))
			if self.dx == 1 or self.dy == -1:
				self.drawSquare(head[0] + self.squareSize - eyeSize - eyePadding, head[1] + eyePadding, eyeSize, (0, 0, 255))
			if self.dy == -1 or self.dx == -1:
				self.drawSquare(head[0] + eyePadding, head[1] + eyePadding, eyeSize, (0, 0, 255))

			tongueColor = (255, 0, 156)
			if self.dx == 1:
				pyglet.graphics.draw(1, GL_POINTS, 
					('v2f', (head[0] + self.squareSize + 1, head[1] + (self.squareSize / 2))),
					('c3B', tongueColor))
				pyglet.graphics.draw(1, GL_POINTS, 
					('v2f', (head[0] + self.squareSize + 2, head[1] + (self.squareSize / 2))),
					('c3B', tongueColor))
				pyglet.graphics.draw(1, GL_POINTS, 
					('v2f', (head[0] + self.squareSize + 3, head[1] + (self.squareSize / 2))),
					('c3B', tongueColor))
				pyglet.graphics.draw(1, GL_POINTS, 
					('v2f', (head[0] + self.squareSize + 4, head[1] + (self.squareSize / 2))),
					('c3B', tongueColor))
				pyglet.graphics.draw(1, GL_POINTS, 
					('v2f', (head[0] + self.squareSize + 5, head[1] + (self.squareSize / 2))),
					('c3B', tongueColor))
				pyglet.graphics.draw(1, GL_POINTS, 
					('v2f', (head[0] + self.squareSize + 6, head[1] + (self.squareSize / 2) - 1)),
					('c3B', tongueColor))
				pyglet.graphics.draw(1, GL_POINTS, 
					('v2f', (head[0] + self.squareSize + 7, head[1] + (self.squareSize / 2) - 2)),
					('c3B', tongueColor))
				pyglet.graphics.draw(1, GL_POINTS, 
					('v2f', (head[0] + self.squareSize + 6, head[1] + (self.squareSize / 2) + 1)),
					('c3B', tongueColor))
				pyglet.graphics.draw(1, GL_POINTS, 
					('v2f', (head[0] + self.squareSize + 7, head[1] + (self.squareSize / 2) + 2)),
					('c3B', tongueColor))
			elif self.dx == -1:
				pyglet.graphics.draw(1, GL_POINTS, 
					('v2f', (head[0] - 1, head[1] + (self.squareSize / 2))),
					('c3B', tongueColor))
				pyglet.graphics.draw(1, GL_POINTS, 
					('v2f', (head[0] - 2, head[1] + (self.squareSize / 2))),
					('c3B', tongueColor))
				pyglet.graphics.draw(1, GL_POINTS, 
					('v2f', (head[0] - 3, head[1] + (self.squareSize / 2))),
					('c3B', tongueColor))
				pyglet.graphics.draw(1, GL_POINTS, 
					('v2f', (head[0] - 4, head[1] + (self.squareSize / 2))),
					('c3B', tongueColor))
				pyglet.graphics.draw(1, GL_POINTS, 
					('v2f', (head[0] - 5, head[1] + (self.squareSize / 2))),
					('c3B', tongueColor))
				pyglet.graphics.draw(1, GL_POINTS, 
					('v2f', (head[0] - 6, head[1] + (self.squareSize / 2) - 1)),
					('c3B', tongueColor))
				pyglet.graphics.draw(1, GL_POINTS, 
					('v2f', (head[0] - 7, head[1] + (self.squareSize / 2) - 2)),
					('c3B', tongueColor))
				pyglet.graphics.draw(1, GL_POINTS, 
					('v2f', (head[0] - 6, head[1] + (self.squareSize / 2) + 1)),
					('c3B', tongueColor))
				pyglet.graphics.draw(1, GL_POINTS, 
					('v2f', (head[0] - 7, head[1] + (self.squareSize / 2) + 2)),
					('c3B', tongueColor))
			elif self.dy == 1:
				pyglet.graphics.draw(1, GL_POINTS, 
					('v2f', (head[0] + (self.squareSize / 2), head[1] + self.squareSize + 1)),
					('c3B', tongueColor))
				pyglet.graphics.draw(1, GL_POINTS, 
					('v2f', (head[0] + (self.squareSize / 2), head[1] + self.squareSize + 2)),
					('c3B', tongueColor))
				pyglet.graphics.draw(1, GL_POINTS, 
					('v2f', (head[0] + (self.squareSize / 2), head[1] + self.squareSize + 3)),
					('c3B', tongueColor))
				pyglet.graphics.draw(1, GL_POINTS, 
					('v2f', (head[0] + (self.squareSize / 2), head[1] + self.squareSize + 4)),
					('c3B', tongueColor))
				pyglet.graphics.draw(1, GL_POINTS, 
					('v2f', (head[0] + (self.squareSize / 2), head[1] + self.squareSize + 5)),
					('c3B', tongueColor))
				pyglet.graphics.draw(1, GL_POINTS, 
					('v2f', (head[0] + (self.squareSize / 2) - 1, head[1] + self.squareSize + 6)),
					('c3B', tongueColor))
				pyglet.graphics.draw(1, GL_POINTS, 
					('v2f', (head[0] + (self.squareSize / 2) - 2, head[1] + self.squareSize + 7)),
					('c3B', tongueColor))
				pyglet.graphics.draw(1, GL_POINTS, 
					('v2f', (head[0] + (self.squareSize / 2) + 1, head[1] + self.squareSize + 6)),
					('c3B', tongueColor))
				pyglet.graphics.draw(1, GL_POINTS, 
					('v2f', (head[0] + (self.squareSize / 2) + 2, head[1] + self.squareSize + 7)),
					('c3B', tongueColor))
			elif self.dy == -1:
				pyglet.graphics.draw(1, GL_POINTS, 
					('v2f', (head[0] + (self.squareSize / 2), head[1] - 1)),
					('c3B', tongueColor))
				pyglet.graphics.draw(1, GL_POINTS, 
					('v2f', (head[0] + (self.squareSize / 2), head[1] - 2)),
					('c3B', tongueColor))
				pyglet.graphics.draw(1, GL_POINTS, 
					('v2f', (head[0] + (self.squareSize / 2), head[1] - 3)),
					('c3B', tongueColor))
				pyglet.graphics.draw(1, GL_POINTS, 
					('v2f', (head[0] + (self.squareSize / 2), head[1] - 4)),
					('c3B', tongueColor))
				pyglet.graphics.draw(1, GL_POINTS, 
					('v2f', (head[0] + (self.squareSize / 2), head[1] - 5)),
					('c3B', tongueColor))
				pyglet.graphics.draw(1, GL_POINTS, 
					('v2f', (head[0] + (self.squareSize / 2) - 1, head[1] - 6)),
					('c3B', tongueColor))
				pyglet.graphics.draw(1, GL_POINTS, 
					('v2f', (head[0] + (self.squareSize / 2) - 2, head[1] - 7)),
					('c3B', tongueColor))
				pyglet.graphics.draw(1, GL_POINTS, 
					('v2f', (head[0] + (self.squareSize / 2) + 1, head[1] - 6)),
					('c3B', tongueColor))
				pyglet.graphics.draw(1, GL_POINTS, 
					('v2f', (head[0] + (self.squareSize / 2) + 2, head[1] - 7)),
					('c3B', tongueColor))

	def drawSquare(self, x, y, size, color):
		glColor3f(color[0], color[1], color[2])
		glBegin(GL_TRIANGLES)
		glVertex2f(x, y)
		glVertex2f(x + size, y)
		glVertex2f(x, y + size)
		glVertex2f(x + size, y)
		glVertex2f(x + size, y + size)
		glVertex2f(x, y + size)
		glEnd()

	def makeFood(self):
		maxXHop = self.width / 15
		maxYHop = self.height / 15
		made = False
		while not made:
			x = self.squareSize * random.randint(1, maxXHop - 2)
			y = self.squareSize * random.randint(1, maxYHop - 2)
			if (x, y) not in self.foods and (x, y) not in self.pieces:
				self.foods.append((x, y))
				made = True

	# window on key press function
	def on_key_press(self, symbol, modifiers):
		if self.gameState == STATE_GAME_RUNNING:
			if (symbol == key.UP or symbol == key.W) and not self.dy == -1 and not self.turned:
				self.dx = 0
				self.dy = 1
				self.turned = True
			elif (symbol == key.DOWN or symbol == key.S) and not self.dy == 1 and not self.turned:
				self.dx = 0
				self.dy = -1
				self.turned = True
			elif (symbol == key.LEFT or symbol == key.A) and not self.dx == 1 and not self.turned:
				self.dx = -1
				self.dy = 0
				self.turned = True
			elif (symbol == key.RIGHT or symbol == key.D) and not self.dx == -1 and not self.turned:
				self.dx = 1
				self.dy = 0
				self.turned = True
		elif self.gameState == STATE_MENU:
			if symbol >= 97 and symbol <= 122:
				self.name.pop(0)
				self.name.append(chr(symbol))
				#self.on_draw()
		if symbol == key.SPACE:
			if self.gameState == STATE_GAME_RUNNING:
				self.gameState = STATE_GAME_PAUSED
			elif self.gameState == STATE_GAME_PAUSED:
				self.gameState = STATE_GAME_RUNNING
			elif self.gameState == STATE_MENU:
				self.gameState = STATE_GAME_RUNNING
				pyglet.clock.schedule_interval(self.mainloop, 1.0 / 10)
		elif symbol == key.ENTER:
			if self.gameState == STATE_MENU:
				self.gameState = STATE_GAME_RUNNING
				pyglet.clock.schedule_interval(self.mainloop, 1.0 / 10)
		elif symbol == key.ESCAPE:
			if self.gameState == STATE_GAME_RUNNING:
				self.gameState = STATE_GAME_PAUSED
			elif self.gameState == STATE_GAME_PAUSED:
				dbCursor.execute("INSERT INTO scores VALUES (?,?,?,?)", (datetime.datetime.now().isoformat(), " ".join(self.name), self.maxSnakeSize, self.foodsAte))
				dbConnection.commit()
				self.makeHSLabels()
				pyglet.clock.unschedule(self.mainloop)
				self.setupGame()
				self.gameState = STATE_MENU

if __name__ == "__main__":
	# create an instance of SnakeGame
	SnakeGame(
		caption="Fredriks Apps - Snake Game",
		width=795, height=600)
	pyglet.app.run() # run the app
	dbConnection.close() # finally, close the DB connection